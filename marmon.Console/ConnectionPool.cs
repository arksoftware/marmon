﻿namespace marmon.Console
{
    public class ConnectionPool
    {
        public const string CommandText = "SELECT @session_count = COUNT(*) FROM sys.dm_exec_sessions WHERE program_name = N'marmon';";
        public const string ConnectionString = "Data Source=IN5574\\DEV12;Initial Catalog=AdventureWorks2014;Integrated Security=SSPI;Application Name=marmon";
        public const string OutputString = "Iteration {0,3:N0} found {1,3:N0} sessions.";
        public const int TestSize = 20;

        public void TestRawConnectionInSerial()
        {
            for (int i = 0; i < TestSize; i++)
            {
                TestRawConnection(i);
            }
        }

        public void TestPoliteConnectionInSerial()
        {
            for (int i = 0; i < TestSize; i++)
            {
                TestPoliteConnection(i);
            }
        }

        public void TestManagedConnectionInSerial()
        {
            var connection = new System.Data.SqlClient.SqlConnection(ConnectionString);
            connection.Open();
            for (int i = 0; i < TestSize; i++)
            {
                TestManagedConnection(i, connection);
            }
            connection.Close();
        }

        public void TestRawConnectionInParallel()
        {
            System.Threading.Tasks.Parallel.For(0, TestSize - 1, i => TestRawConnection(i));
        }

        public void TestPoliteConnectionInParallel()
        {
            System.Threading.Tasks.Parallel.For(0, TestSize - 1, i => TestPoliteConnection(i));
        }

        public void TestManagedConnectionInParallel()
        {
            var connection = new System.Data.SqlClient.SqlConnection(ConnectionString);
            connection.Open();
            System.Threading.Tasks.Parallel.For(0, TestSize - 1, i => TestManagedConnection(i, connection));
            connection.Close();
        }

        private System.Data.SqlClient.SqlCommand GetCommand(System.Data.SqlClient.SqlConnection connection)
        {
            var result = connection.CreateCommand();
            result.CommandText = CommandText;
            result.CommandType = System.Data.CommandType.Text;
            result.Parameters.Add(new System.Data.SqlClient.SqlParameter("@session_count", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            });
            return result;
        }

        private void TestRawConnection(int iteration)
        {
            var connection = new System.Data.SqlClient.SqlConnection(ConnectionString);
            connection.Open();
            var command = GetCommand(connection);
            command.ExecuteNonQuery();
            var count = (int)command.Parameters[0].Value;
            System.Console.WriteLine(OutputString, iteration, count);
        }

        private void TestPoliteConnection(int iteration)
        {
            var count = 0;
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = GetCommand(connection))
                {
                    command.ExecuteNonQuery();
                    count = (int)command.Parameters[0].Value;
                }
                connection.Close(); // Unnecessary due to the using block
            }
            System.Console.WriteLine(OutputString, iteration, count);
        }

        private void TestManagedConnection(int iteration, System.Data.SqlClient.SqlConnection connection)
        {
            var command = GetCommand(connection);
            command.ExecuteNonQuery();
            var count = (int)command.Parameters[0].Value;
            System.Console.WriteLine(OutputString, iteration, count);
        }
    }
}
