﻿namespace marmon.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Ready?");
            System.Console.ReadLine();

            var start = System.DateTime.UtcNow;

            TestConnectionPool();
            //TestDataReader();
            //TestExceptions();
            //TestBulkCopy();
            
            var duration = System.DateTime.UtcNow.Subtract(start).TotalMilliseconds;
            System.Console.WriteLine("Total Duration = {0,4:N0}ms", duration);
            System.Console.ReadLine();
        }

        private static void TestConnectionPool()
        {
            var cp = new ConnectionPool();

            cp.TestRawConnectionInSerial();

            //cp.TestPoliteConnectionInSerial();

            //cp.TestManagedConnectionInSerial();

            //cp.TestRawConnectionInParallel();

            //cp.TestPoliteConnectionInParallel();

            //cp.TestManagedConnectionInParallel();
        }

        private static void TestDataReader()
        {
            var persons = Person.GetPersonCollection();

            //var persons = Person.GetFasterPersonCollection();

            System.Console.WriteLine("Loaded {0} Persons.", persons.Count);
        }

        private static void TestBulkCopy()
        {
            BulkCopy.LoadTable();
        }

        private static void TestExceptions()
        {
            Exceptions.Throw();
        }
    }
}
