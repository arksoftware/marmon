IF (OBJECT_ID(N'dbo.ThrowExceptions') IS NULL)
	EXEC (N'CREATE PROCEDURE [dbo].[ThrowExceptions] AS SELECT 0;');
GO
ALTER PROCEDURE [dbo].[ThrowExceptions]
AS
BEGIN
	RAISERROR('The first error', 10, 1) WITH NOWAIT;
	EXEC(N'RAISERROR(''The second error'', 15, 1);');
	SELECT 2 / 0;
	THROW 50000, 'The fourth error', 1;
	RAISERROR('The fifth error', 16, 1);
END; --PROCEDURE
GO
