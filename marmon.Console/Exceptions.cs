﻿namespace marmon.Console
{
    public class Exceptions
    {
        public const string ConnectionString = "Data Source=IN5574\\DEV12;Initial Catalog=AdventureWorks2014;Integrated Security=SSPI;Application Name=marmon";

        public static void Throw()
        {
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "dbo.ThrowExceptions";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        System.Console.WriteLine(e.ToString());
                        foreach (System.Data.SqlClient.SqlError error in e.Errors)
                        {
                            System.Console.WriteLine("-- Error {0}, '{1}', occurred in {2} at line {3}, severity {4}", error.Number, error.Message, error.Procedure, error.LineNumber, error.Class);
                        }
                    }
                }
                connection.Close();
            }
        }
    }
}
