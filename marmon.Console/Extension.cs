﻿namespace marmon.Console
{
    public static class Extension
    {
        public static object FromDbNull(this object val)
        {
            return val == System.DBNull.Value ? null : val;
        }

        public static object ToDbNull(this System.Xml.Linq.XAttribute val)
        {
            return val == null ? System.DBNull.Value : (object)val.Value;
        }
    }
}
